# Prerrequisito
En el entorno virtual:
```bash
pip list
```
    Package       Version
    ------------- -------
    pip           20.0.2
    pkg-resources 0.0.0
    pymongo       4.1.0
    setuptools    44.0.0
    MongoDB shell 3.6.8

# Instalar pip para python3

Antes de la instalación del entorno virtual, instalemos pip. Pip es un administrador de paquetes que ayuda a instalar, desinstalar y actualizar paquetes para sus proyectos.

Para instalar pip para python 3 escriba:
```bash
apt install python3-pip
```
Vamos primero instalar venv paquete usando el siguiente comando:

```bash
apt-get install python3-venv
```
Creamos el entorno
```bash
python3 -m venv venv=my_env_project
```
Activamos el entorno de trabajo
```bash
source my_env_project/bin/activate
```

# MongoDB

Es un motor de base de datos NoSQL basado en documentos. Esto quiere decir que Mongo guarda los datos en estructuras parecidas a un JSON

## Instalación
Desde una terminal, emita el siguiente comando para importar la clave GPG pública de [MongoDB](https://www.mongodb.org/static/pgp/server-5.0.asc).


```bash
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
```

La siguiente instrucción es para Ubuntu 20.04 (Focal) .

Cree el /etc/apt/sources.list.d/mongodb-org-5.0.list archivo para Ubuntu 20.04 (Focal):

```bash
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
```

Ejecute el siguiente comando para recargar la base de datos del paquete local:

```bash
sudo apt-get update
```

Para instalar la última versión estable, emita lo siguiente:

```bash
sudo apt-get install -y mongodb-org
```

Para iniciar MongoDB:

```bash
sudo service mongodb start

mongod --repair

mongo
```

# PyMongo
[PyMongo](https://pymongo.readthedocs.io/en/stable/) es una distribución de Python que contiene herramientas para trabajar con MongoDB y es la forma recomendada de trabajar con MongoDB desde Python. 

## Instalando con pip
Recomendamos usar pip para instalar pymongo en todas las plataforma:
```bash
python3 -m pip install pymongo
```
Para obtener una versión específica de pymongo:
```bash
python3 -m pip install pymongo==3.5.1
```
Para actualizar usando pip:
```bash
python3 -m pip install --upgrade pymongo
```


## Uso 

```python
import pymongo

```

## Compilación
Para ejecutar el programa debe tener activado el entorno virtual y escribir en su terminal:

```bash
python3 1.py 
```

## ¿Cómo funciona?
Al ejecutar el programa este abrirá un menú:
![image.png](./image.png)

En donde:

La opción [1] podrá poner en venta su vehículo siempre y cuando usted este afiliado a la automotora, para estar afiliado debe ingresar el formulario de ingresos de clientes.

La opción [2] usted podrá visualizar los vehículos que se encuentren disponibles y comprar el vehículo escribiendo su patente.

La opción [3] permite solo visualizar los vehículos disponibles.

La opción [4] permite dejar de estar afiliado a la automotora.

La opción [5] permite afiliarse a la automotora.

En la opción [6] usted podra editar su información de cliente.

Si usted puso en venta un vehículo en la opción [7] podrá eliminarlo del mercado.

La opción [8] permite ver los vehículos disponibles y no disponibles.

