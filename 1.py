#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys , time
from pymongo import MongoClient
from pprint import pprint

# Dirección del servidor de mongo mongodb://127.0.0.1:27017
mongo = 'mongodb://127.0.0.1:27017'
# conexion con la bd
client = MongoClient(mongo)

# nombre de la base de datos
db = client['fake']

# nombre de la colección para registrar vehículos
autos = db['autos']

# nombre de la colección para registrar clientes
clientes = db['clientes']

def redirigiendo(st):
    cleaning()
    time.sleep(1)
    for i in range(3):
        time.sleep(0.9)
        print(f"Cargando datos de {st}.")
    time.sleep(1)
    cleaning()

def cleaning():
    if sys.platform.startswith('win'):
        os.system('cls')
    elif sys.platform.startswith('linux'):
        os.system('clear')
    print ("###################################")

def formularioIngresoAutos():
    print("\n")
    rut = input("Rut afilidado:")
    modelo = input("Modelo auto: ")
    Kilometraje = input("Kilometraje:")
    monto1= input("Primer monto: ")
    ano = input("Año del auto: ")
    patente = input("Patente registada: ")
    disponibilidad = "si"

    asd = clientes.find_one({"rut": rut} , {"_id": 0 })
    if (asd is not None):
        # validar patente, si el vehículo ya existe.
        asd = autos.find_one({"patente": patente} , {"_id": 0 })
        if (asd is not None):
            print ("UPS, La patente ingresada ya existe en nuestra base de datos.")
            time.sleep(2)
            cleaning()
            menu()
        else:
            autos.insert_one({"patente": patente, "rut": rut, "modelo": modelo,
                            "km": Kilometraje, "monto_inicial": monto1, "disponibilidad": disponibilidad,
                            "año": ano})
            print ("\n")
            opt = int(input("[1. ] Ingresar otro Vehículo.\n[2. ] Formulario ingreso clientes.\n[3. ] Salir\n"))
            try:
                int(opt)
                if (opt == 1):
                    formularioIngresoAutos()
                elif (opt == 2):
                    formularioIngresoClientes()
                elif (opt == 3):
                    quit()
                elif (opt > 3):
                    cleaning()
                    print ("No se entiende.")
                    redirigiendo("ventas")
                    venta()
                elif (opt < 1):
                    cleaning()
                    redirigiendo("ventas")
                    venta()
            except ValueError:
                print ("ingrese un valor válido.")
                redirigiendo("ventas")
                venta()
    else:
        print("Por favor registrese como CLIENTE antes de vender su auto, ¡GRACIAS!")
        time.sleep(1)
        redirigiendo("Ventas.")
        venta()

def formularioIngresoClientes():
    print("\n")
    nombre = input("Nombre: ")
    rut = input("Rut: ")
    numero = input("Teléfono: ")
    # validar que ya existe un rut.
    asd = clientes.find_one({"rut": rut} , {"_id": 0 })
    if (asd is not None):
        print ("UPS, El cliente ya existe en nuestra base de datos.")
        time.sleep(2)
        cleaning()
        menu()
    else:
        #########
        clientes.insert_one({"nombre": nombre, "rut": rut, "numero": numero})
        print ("\n")
        opt = int(input("[1. ] Ingresar otro cliente.\n[2. ] Formulario venta vehículo.\n[3. ] Salir\n"))
        try:
            int(opt)
            if (opt == 1):
                formularioIngresoClientes()
            elif (opt == 2):
                formularioIngresoAutos()
            elif (opt == 3):
                quit()
            elif (opt > 3):
                cleaning()
                print ("No se entiende.")
                venta()
            elif (opt < 1):
                cleaning()
                print ("No se entiende.")
                venta()
        except ValueError:
            print ("ingrese un valor válido.")
            venta()

def venta():
    print ("Hola! para poner en venta su vehículo debe primer registrar sus datos. \n")
    print ("\nRECUERDE: \nSi usted está registrado como cliente, sólo debe hacer el formulario '2'.\n\n ")
    opt = int(input("[1. ] Formulario clientes.\n[2. ] Formulario venta vehículo.\n[3. ] Volver al menú.. "))
    try:
        int(opt)
        if (opt == 1):
            formularioIngresoClientes()
        elif (opt == 2):
            formularioIngresoAutos()
        elif (opt == 3):
            quit()
        elif (opt > 3):
            cleaning()
            print ("No se entiende.")
            menu()
        elif (opt < 1):
            cleaning()
            print ("No se entiende.")
            venta()
    except ValueError:
        print ("ingrese un valor válido.")
        venta()

def view(out):
    cleaning()
    redirigiendo("Nuestro catálogo.")
    if ( autos.find_one({"disponibilidad": "si"} , {"_id": 0 }) is not None):
        print ("Nuestros autos disponibles son: ")
        for x in autos.find({"disponibilidad": "si"} , {"_id": 0, "disponibilidad": 0}):
            print ("#####################")
            print ("-.")
            pprint (x)
            print ("\n")
    else:
        print ("No tenemos autos disponibles actualmente. ")
        time.sleep(2)
        print ("\n")
        menu()

    if (out == 2):
        opt = input("Ingrese patente del vehículo que desea comprar: \n")
        # verificando si el vehículo existe.
        asd = autos.find_one({"patente": str(opt)} , {"_id": 0 })
        if (asd is not None):
            # vender auto.
            # se actualiza la disponibilidad a un no.
            autos.update_one({ "patente": str(opt) },{ "$set": { "disponibilidad": "no" } })
            print ("¡vendido!")
            time.sleep(1)
            cleaning()
            print ("actualizando catalogo ...")
            time.sleep(1)
            menu()
        else:
            print ("No existe vehículo con esa patente.\n Regresando a menú.")
            time.sleep(1)
            menu()

    elif (out == 3):
        # Cotizar un auto
        opt = str(input("Ingrese b para volver. \n"))
        try:
            if (opt == 'b' or opt == 'B'):
                cleaning()
                menu()
            else:
                print ("ingrese un valor válido.")
        except ValueError:
            print ("ingrese un valor válido.")
            menu()

def desuscribirse():
    cleaning()
    redirigiendo("Clientes.")
    opt = str(input("Ingrese rut afiliado: "))
    try:
        asd = clientes.find_one({"rut": opt} , {"_id": 0 })
        if (asd is not None):
            cleaning()
            redirigiendo("de validación. ")
            print ("El RUT ingresado corresponde a un afiliado según nuestro sistema,  ")
            print ("si desea seguir con el proceso de desuscripción los autos puestos a la venta \nSE ELIMINARÁN.")
            aux = input("¿Desea continuar? s/n\n")
            if (aux == "s" or opt == "S"):
                clientes.delete_many({ "rut": opt})
                autos.delete_many({ "rut": opt})
                for i in range(4):
                    time.sleep(0.9)
                    print(f"Eliminando cliente y autos de los registros del cliente {opt}...")
                time.sleep(1)
            elif (opt == "n" or opt == "N"):
                redirigiendo("Menú.")
                cleaning()
                menu()
        else:
            print ("El RUT ingresado no corresponde a un afiliado según nuestro sistema. ")
            redirigiendo("Menú.")
            menu()

    except ValueError:
        print ("ingrese un valor válido.")
        menu()

def actualizarcliente():
    cleaning()
    print ("Para empezar actualizar sus datos, ingrese su rut porfavor.")
    opt = int(input("Ingrese RUT afiliado: \n "))
    try:
        asd = clientes.find_one({"rut": str(opt)} , {"_id": 0 })
        if (asd is not None):
            print ("El RUT ingresado corresponde a un afiliado según nuestro sistema,  ")
            aux = input("¿Desea continuar? s/n\n")
            if (aux == "s" or aux == "S"):
                print(f"Cargando datos del cliente de rut {opt}.")
                print ("Sus datos actuales son: \n")
                for x in clientes.find({"rut": str(opt)} , {"_id": 0 }):
                    pprint (x)
                print ("\n")
                print ("Porfavor, rellene el formulario nuevamente para actualizar sus datos.- ")
                nombre = input("Nombre: ")
                numero = input("Teléfono: ")

                clientes.update_one({ "rut": str(opt) },{ "$set": { "nombre": nombre } })
                clientes.update_one({ "rut": str(opt) },{ "$set": { "numero": numero } })
                redirigiendo("Actualizados.")
                menu()

            elif (aux == "n" or aux == "N"):
                redirigiendo("Menú.")
                cleaning()
                menu()
        else:
            print ("El RUT ingresado no corresponde a un afiliado según nuestro sistema. ")
            time.sleep(0.9)
            redirigiendo("Menú.")
            menu()

    except ValueError:
        print ("ingrese un valor válido.")
        menu()

def eliminarauto():
    redirigiendo("Panel de control de autos.")
    cleaning()
    opt = input("Verifique su identidad: \n Ingrese RUT:\n")
    asd = clientes.find_one({"rut": str(opt)} , {"_id": 0 })
    if (asd is not None):
        print ("Bienvenido: ")
        # validando que el cliente tenga vehículos.
        if ( autos.find_one({"disponibilidad": "si"} , {"_id": 0 }) is not None):
            redirigiendo(f"autos de {opt}")
            time.sleep(2)
            cleaning()
            for x in autos.find({"disponibilidad": "si"} , {"_id": 0, "disponibilidad": 0}):
                print ("#####################")
                print ("-.")
                pprint (x)
                print ("\n")
            opt = input("Ingrese patente del vehículo que desea eliminar: \n")
            # verificando si el vehículo existe.
            asd = autos.find_one({"patente": str(opt)} , {"_id": 0 })
            if (asd is not None):
                autos.delete_one({"patente": opt})
                print ("¡Eliminado con éxito!")
                time.sleep(1)
                menu()
            else:
                print ("No existe vehículo con esa patente.\n Regresando a menú.")
                time.sleep(1)
                menu()
        else:
            print ("Usted no tiene vehículos puestos a la venta.\n\n")
            menu()
    else:
        print ("Usted debe:\n Ingresar como cliente para posteriormente poner a la venta sus vehículos. ")
        redirigiendo("menú. ")

def historial():
    redirigiendo("los vehículos")
    if ( autos.find({} , {"_id": 0 }) is not None):
        print ("Nuestros autos son: ")
        for x in autos.find({} , {"_id": 0}):
            print ("#####################")
            print ("-.")
            pprint (x)
            print ("\n")
    else:
        print ("No tenemos autos disponibles actualmente. ")
        time.sleep(2)
        print ("\n")
        menu()

    opt = str(input("Ingrese b para volver. \n"))
    try:
        if (opt == 'b' or opt == 'B'):
            cleaning()
            menu()
        else:
            print ("ingrese un valor válido.")
    except ValueError:
        print ("ingrese un valor válido.")
        menu()

def menu():
    print ("Usted desea:\n[1]-Vender su auto\n[2]-Comprar un auto\n[3]-Cotizar un auto, solo disponible\n[4]-Dejar de ser cliente")
    opt = int(input("[5]-Ser cliente\n[6]-Editar cliente\n[7]-Eliminar mi auto del mercado\n[8]-Todos los vehiculos, disponibles y no disponibles\n[9]-Salir\n"))
    try:
        int(opt)
        if (opt == 1):
            redirigiendo("ventas")
            venta()
        elif (opt == 2):
            view(2)
        elif (opt == 3):
            view(3)
        elif (opt == 4):
            desuscribirse()
        elif (opt == 5):
            redirigiendo("Formulario de ingreso cliente. ")
            cleaning()
            formularioIngresoClientes()
        elif (opt == 6):
            actualizarcliente()
        elif (opt == 7):
            eliminarauto()
        elif (opt == 8):
            historial()
        elif (opt == 9):
            quit()
        elif (opt > 9):
            cleaning()
            print ("No se entiende.")
            menu()
        elif (opt < 1):
            cleaning()
            print ("No se entiende.")
            menu()
    except ValueError:
        print ("ingrese un valor válido.")
        menu()

# Se crea el main.
if __name__ == "__main__":
    try:
        print ("*-Bienvenido a la Automotora Torito -*\n")
        menu()
    except KeyboardInterrupt:
        print ("\n")
